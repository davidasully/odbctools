#' Unlock the Keyring
#'
#' This function enables retrieval of passwords saved in the credential store.
#' @return Changes keyring status to unlocked.
#' @seealso \code{\link[keyring]{keyring_unlock}} which this function wraps
#' @export
#'
#' @examples
#' \dontrun{
#' unlockKeyring()
#' }
unlockKeyring <- function() {
  keyring::keyring_unlock(keyring = "odbctools")
}
