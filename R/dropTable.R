#' Drop Table
#'
#' Remove/drop a table from the database.
#' @param con A valid connection
#' @param schema Schema to locate
#' @param name Name of table to be dropped
#' @return Drops a remote table
#' @seealso \code{\link[DBI]{dbRemoveTable}} which this function wraps
#' @export
#'
#' @examples
#' \dontrun{
#' dropTable(con, "schema_name", "table_name")
#' }
dropTable <- function(con, schema, name) {
  table_id <- DBI::Id(schema = schema, name = name)
  odbc::dbRemoveTable(conn = con, name = table_id)
}
