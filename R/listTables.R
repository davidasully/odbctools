#' List Tables on Connection
#'
#' A function that lists all available tables in database regardless of schema.
#' @param con A valid connection
#'
#' @return Returns a vector of names of tables
#' @seealso \code{\link[odbc]{dbListTables}} which this function wraps
#' @export
#'
#' @examples
#' \dontrun{
#' listTables(con)
#' }
listTables <- function(con) {
  odbc::dbListTables(conn = con)
}
