#' Create a View
#'
#' Creates a view using the supplied query.
#' @param con A valid connection
#' @param schema Schema to locate
#' @param name Name of view to be dropped
#' @param vw Query to be added as a view
#' @param overwrite Overwrite existing view? Defaults to FALSE.
#'
#' @return Creates a view on the database
#' @export
#'
#' @examples
#' \dontrun{
#' createView(con, "schema_name", "view_name", sqlImport("view.sql"))
#' }
createView <- function(con, schema, name, vw, overwrite = FALSE) {

  exists <- DBI::dbExistsTable(conn = con, name = name)

  if(exists & overwrite) {
    dropView(con, schema, name)

  } else if (exists & !overwrite) {
    stop("View already exists. Set argument 'overwrite' to TRUE.")

  }

  sql <- glue::glue_sql(
    paste0("CREATE VIEW {`schema`}.{`name`} AS \n", vw),
    .con = con)

  DBI::dbExecute(conn = con,
                 statement = sql)

}
