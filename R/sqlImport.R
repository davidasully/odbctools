#' Import SQL from File
#'
#' Read in a SQL statement from a text file (e.g. \emph{.sql}).
#' @param file Path to file
#' @return Returns the text of the query that can be passed to \code{\link{sqlPull}}
#' @export
#'
#' @examples
#' \dontrun{
#' sql <- sqlImport("query.sql")
#' }
sqlImport <- function(file) {
  #Read in text from SQL file.
  sql_lines <- paste(suppressWarnings(readLines(file)), collapse = "\n")
  return(sql_lines)
}
