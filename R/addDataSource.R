#' Add a New Entry to the Config File
#'
#' This function adds a new data source to your configuration file.
#' @param alias A string that will be the name of an entry in your configuration file.
#' @param service The name of the credential group retrieved from keyring.
#' @param constr A valid connection string.  Defaults to NULL.
#' @param overwrite Overwrite an existing entry in the file? Defaults to FALSE.
#' @param file Override the default path to file.
#' @param ... These parameters will be added to the configuration file and used later to build a connection.
#'
#' @return Updates the configuration file.
#' @export
#'
#' @examples
#' \dontrun{
#' addDataSource(alias = "DBNAME",
#'               service = "DBGROUP",
#'               constr = "connectionstring")
#'
#' addDataSource(alias = "DBNAME",
#'               service = "DBGROUP",
#'               driver = "{Name of driver}",
#'               server = "server.someplace.com",
#'               port = 2500,
#'               database = "DBName")
#'}
addDataSource <- function(alias, service, constr = NULL, overwrite = FALSE, file = NULL, ...) {
  file_loc <- odbctools::findConfig(file = file)
  curconfig <- yaml::yaml.load_file(input = file_loc, as.named.list = TRUE)
  if(!is.null(curconfig[[alias]])[[1]] & !overwrite) {
    stop("Alias already exists. Set overwrite to TRUE.")
  } else {
    curconfig <- curconfig[names(curconfig) != alias]
  }
  if(!is.null(constr)) {
    newsource <- list(list(constr = constr, service = service))
  } else {
    newsource <- list(c(list(...), list(service = service)))
  }
  names(newsource) <- alias
  newconfig <- c(curconfig, newsource)
  newconfig <- newconfig[order(names(newconfig))]
  writeLines(yaml::as.yaml(newconfig), con = file_loc)
}
